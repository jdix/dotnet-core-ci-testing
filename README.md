# readme

```bash
# dotnet run --project MyAppJdix531
dotnet pack MyAppJdix531 --output MyAppJdix531/package
# ./MyApp/package/MyApp.1.0.0.nupkg
APP_VERSION=1.0.0 dotnet nuget push MyAppJdix531/package/MyAppJdix531.${APP_VERSION}.nupkg --api-key ${NUGET_API_KEY} --source https://api.nuget.org/v3/index.json
```
